#########################################
## shared VPC
#########################################
variable "sas_shared_vpc_cidr" {
    description = "The CIDR block for the VPC. Default value is a valid CIDR, but not acceptable by AWS and should be overridden"
    type        = string
    default     = "10.255.74.0/24"
  }
  
  variable "sas_shared_sub_2a_cidr" {
    description = "Subnet in 2a"
    type        = string
    default     = "10.255.74.0/26"
  }
  
  variable "sas_shared_sub_2b_cidr" {
    description = "Subnet in 2b"
    type        = string
    default     = "10.255.74.128/26"
  }

#########################################
## shared SG
#########################################
#################
# Security group
#################


variable "sas_shared_sg_name" {
  description = "Name of security group - not required if create_sg is false"
  type        = string
  default     = "sas_shared_sg"
}

variable "sas_shared_sg_tags" {
  description = "A mapping of tags to assign to security group"
  type        = map(string)
  default = {
    Environment = "shared"
    Resource    = "sg"
  }
}
##########
# Ingress
##########
variable "sas_shared_sg_ingress_with_cidr_blocks" {
  description = "List of ingress rules to create where 'cidr_blocks' is used"
  type        = list(map(string))
  default     = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = -1
      cidr_blocks = "10.255.74.0/24"
      #cidr_blocks = module.vpc.vpc_cidr_block
    },
    {
      from_port   = 6501
      to_port     = 6501
      protocol    = "tcp"
      cidr_blocks = "10.255.72.0/24"
      #cidr_blocks = module.vpc.vpc_cidr_block
    },
    {
      from_port   = 7501
      to_port     = 7501
      protocol    = "tcp"
      cidr_blocks = "10.255.72.0/24"
      #cidr_blocks = module.vpc.vpc_cidr_block
    },
     {
      from_port   = 6501
      to_port     = 6501
      protocol    = "tcp"
      cidr_blocks = "10.255.73.0/24"
      #cidr_blocks = module.vpc.vpc_cidr_block
    },
    {
      from_port   = 7501
      to_port     = 7501
      protocol    = "tcp"
      cidr_blocks = "10.255.73.0/24"
      #cidr_blocks = module.vpc.vpc_cidr_block
    },    
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = "10.40.0.0/16"
      #cidr_blocks = module.vpc.vpc_cidr_block
    },
  ]
}
#########
# Egress
#########
variable "sas_shared_sg_egress_with_cidr_blocks" {
  description = "List of egress rules to create where 'cidr_blocks' is used"
  type        = list(map(string))
  default     = []
}

#




