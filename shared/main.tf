# Terraform configuration
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

# shared account owns the VPC and creates the VPC attachment.
provider "aws" {
  region     = var.aws_region
  access_key = var.aws_sharedaccount_access_key
  secret_key = var.aws_sharedaccount_secret_key
}

provider "aws" {
  alias = "tgw"

  region     = var.aws_region
  access_key = var.aws_tgw_access_key
  secret_key = var.aws_tgw_secret_key
}




#########################################
## shared VPC
#########################################
resource "aws_vpc" "sas_shared_vpc" {
  cidr_block           = var.sas_shared_vpc_cidr #"10.255.74.0/24"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Env     = "shared"
    Project = "SAS"
    Service = "vpc"
  }
}

resource "aws_subnet" "sas_shared_sub_2a" {
  vpc_id                                      = aws_vpc.sas_shared_vpc.id
  cidr_block                                  = var.sas_shared_sub_2a_cidr #"10.255.74.0/26" 
  availability_zone                           = "ap-southeast-2a"
  enable_resource_name_dns_a_record_on_launch = true
  #private_dns_hostname_type_on_launch         = resource-name
  tags = {
    Env     = "shared"
    Project = "SAS"
    Service = "sub2a"
  }
}

resource "aws_subnet" "sas_shared_fsx_sub_2a" {
  vpc_id                                      = aws_vpc.sas_shared_vpc.id
  cidr_block                                  = var.sas_shared_fsx_sub_2a_cidr 
  availability_zone                           = "ap-southeast-2a"
  enable_resource_name_dns_a_record_on_launch = true
  #private_dns_hostname_type_on_launch         = resource-name
  tags = {
    Env     = "shared"
    Project = "SAS"
    Service = "sub2a"
  }
}

resource "aws_default_route_table" "sas_shared_rt" {
  default_route_table_id = aws_vpc.sas_shared_vpc.default_route_table_id

  route {
    cidr_block         = "0.0.0.0/0"
    transit_gateway_id = var.transit_gateway_id #to be provided by Umer
  }

  tags = {
    Env     = "shared"
    Project = "SAS"
    Service = "rt"
  }
}

resource "aws_route_table_association" "sas_shared_rt_association" {
  subnet_id      = aws_subnet.sas_shared_sub_2a.id
  route_table_id = aws_default_route_table.sas_shared_rt.id
}

#########################################
## shared SG
#########################################
module "sas_shared_security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "4.8.0"

  name   = var.sas_shared_sg_name
  vpc_id = module.sas_shared_vpc.vpc_id

  ingress_with_cidr_blocks = var.sas_shared_sg_ingress_with_cidr_blocks
  egress_with_cidr_blocks  = var.sas_shared_sg_egress_with_cidr_blocks

  tags = var.sas_shared_sg_tags
}

#########################################
## Allocate the lashared Redhat AMI
#########################################
data "aws_ami" "rhel7_9" {
  most_recent = true

  owners = ["309956199498"] // Red Hat's account ID.

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "name"
    values = ["RHEL-7.9*"]
  }
}

########################################
## OVD Instance
#########################################
data "aws_kms_key" "kms" {
  key_id = "aa240d74-fc60-4ac7-83d6-f06625a5f971"
}


module "sas_ovd_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "sas_ovd_instance"

  ami           = data.aws_ami.rhel7_9.id
  instance_type = "r5d.xlarge" #"r5dn.xlarge"  Error launching source instance: Unsupported: The requested configuration is currently not supported. Please check the documentation for supported configurations.status code: 400, request id: b29cb6bc-3b73-4e6c-bfa0-0c0261bbe49f

  vpc_security_group_ids = [module.sas_shared_security_group.security_group_id]
  availability_zone      = "ap-southeast-2a"
  subnet_id                  = aws_subnet.sas_shared_sub_2a.id  
  #placement_group        = aws_placement_group.sas_shared_placement_group.id
  #cpu_core_count         = 4
  #cpu_threads_per_core   = 2

  root_block_device = [
    {
      encrypted  = true
      kms_key_id = data.aws_kms_key.kms.id
    }
  ]

  #key_name = aws_key_pair.generated_key.key_name
  key_name = "key_pair_name"

  tags = {
    Terraform   = "true"
    Environment = "shared"
    Project     = "sas"
    Server      = "ovd"
  }
}

########################################
## AD Instance
#########################################
module "sas_ad_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "sas_ad_instance"

  ami           = data.aws_ami.rhel7_9.id
  instance_type = "r5d.xlarge" #"r5dn.xlarge"  Error launching source instance: Unsupported: The requested configuration is currently not supported. Please check the documentation for supported configurations.status code: 400, request id: b29cb6bc-3b73-4e6c-bfa0-0c0261bbe49f

  vpc_security_group_ids = [module.sas_shared_security_group.security_group_id]
  availability_zone      = "ap-southeast-2a"
  subnet_id                  = aws_subnet.sas_shared_sub_2a.id  
  #placement_group        = aws_placement_group.sas_shared_placement_group.id
  #cpu_core_count         = 4
  #cpu_threads_per_core   = 2

  root_block_device = [
    {
      encrypted  = true
      kms_key_id = data.aws_kms_key.kms.id
    }
  ]

  #key_name = aws_key_pair.generated_key.key_name
  key_name = "key_pair_name"

  tags = {
    Terraform   = "true"
    Environment = "shared"
    Project     = "sas"
    Server      = "ad"
  }
}


#########################################
## Tanzu Access
#########################################

data "aws_iam_policy" "tanzu_policy" {
  arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}

resource "aws_iam_role" "tanzu_access" {
  name = "tanzu_access"

  assume_role_policy = file("tanzu-assume-policy.json")
}

resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.tanzu_access.name
  policy_arn = data.aws_iam_policy.tanzu_policy.arn
}



#########################################
## Secure State Access
#########################################

data "aws_iam_policy" "secure_state_policy" {
  arn = "arn:aws:iam::aws:policy/SecurityAudit"
}

resource "aws_iam_role" "secure_state_access" {
  name = "secure_state_access"

  assume_role_policy = file("SecureState-assume-policy.json")
}

resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.secure_state_access.name
  policy_arn = data.aws_iam_policy.secure_state_policy.arn
}


#########################################
## Backup Solution
#########################################
data "aws_kms_key" "backupkey" {
  key_id = "aa240d74-fc60-4ac7-83d6-f06625a5f971"
}

resource "aws_backup_vault" "sas_backup_vault" {
  name        = "sas_backup_vault"
  kms_key_arn = data.aws_kms_key.backupkey.arn
}

resource "aws_backup_plan" "sas_backup_plan" {
  name = "sas_backup_plan"

  rule {
    rule_name         = "sas_backup_rule"
    target_vault_name = aws_backup_vault.sas_backup_vault.name
    schedule          = "cron(0 22 * * ? *)"
  }
}

resource "aws_iam_role" "backup_iam_role" {
  name               = "backup_iam_role"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": ["sts:AssumeRole"],
      "Effect": "allow",
      "Principal": {
        "Service": ["backup.amazonaws.com"]
      }
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "backup_policy_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"
  role       = aws_iam_role.backup_iam_role.name
}

resource "aws_backup_selection" "backup_resources" {
  iam_role_arn = aws_iam_role.backup_iam_role.arn
  name         = "backup_resources"
  plan_id      = aws_backup_plan.sas_backup_plan.id

  resources = [
    module.sas_ad_instance.arn
  ]
}
#########################################
## Integration with Splunk
#########################################
resource "aws_cloudtrail" "sas_shared_trail" {
  name                          = "sas_shared_trail"
  s3_bucket_name                = aws_s3_bucket.aaaa.id
  s3_key_prefix                 = "prefix"
}

#########################################
## TGW Integration
#########################################
data "aws_caller_identity" "current" {
  provider = aws
}

resource "aws_ec2_transit_gateway" "tgw" {
  provider = aws.tgw
}

resource "aws_ram_resource_share" "tgw_share" {
  provider = aws.tgw
  name = "tgw_share"
}

# Share the transit gateway...
resource "aws_ram_resource_association" "tgw_association" {
  provider = aws.tgw
  resource_arn       = aws_ec2_transit_gateway.tgw.arn
  resource_share_arn = aws_ram_resource_share.tgw_share.id
}

/////////////////////////////////////
# ...with the second account.
resource "aws_ram_principal_association" "tgw_principal" {
  provider = aws.tgw

  principal          = data.aws_caller_identity.current.account_id
  resource_share_arn = aws_ram_resource_share.tgw_share.id
}

# Create the VPC attachment in the second account...
resource "aws_ec2_transit_gateway_vpc_attachment" "tgw_vpc_attachment" {
  provider = aws
  subnet_ids         = [aws_subnet.example.id]
  transit_gateway_id = "tgw-034e0b7373caaae80" #Umer to Provide it then import
  vpc_id             = aws_vpc.example.id
}

# ...and accept it in the first account.
resource "aws_ec2_transit_gateway_vpc_attachment_accepter" "tgw_attachment_accept" {
  provider = aws.tgw
  transit_gateway_attachment_id = aws_ec2_transit_gateway_vpc_attachment.tgw_vpc_attachment.id
}











#########################################
## DNS Server Aliases (Private Hosted Zone)
#########################################
#Private Hosted Zone
resource "aws_route53_zone" "private" {
  name = "edw.health"

  vpc {
    vpc_id = module.sas_shared_vpc.vpc_id
  }
}

resource "aws_route53_vpc_association_authorization" "example" {
  vpc_id  = module.sas_prd_vpc.vpc_id
  zone_id = aws_route53_zone.private.id
}

resource "aws_route53_zone_association" "example" {
  provider = "aws.alternate"

  vpc_id  = aws_route53_vpc_association_authorization.example.vpc_id
  zone_id = aws_route53_vpc_association_authorization.example.zone_id
}
#Private Hosted Zone Records

# resource "aws_route53_record" "sharedmeta" {
#   zone_id = aws_route53_zone.private.zone_id
#   name    = "sharedmeta"
#   type    = "A"

#   alias {
#     name                   = module.nlb_meta_shared.lb_dns_name
#     zone_id                = module.nlb_meta_shared.lb_zone_id
#     evaluate_target_health = true
#   }
# }


#########################################
## Route53 Resolver Solution
#########################################
#Endpoint Inbound
resource "aws_route53_resolver_endpoint" "sas_dns_endpoint_in" {
  name      = "sas_dns_endpoint_in"
  direction = "INBOUND"

  security_group_ids = [module.sas_shared_security_group.security_group_id]

  ip_address {
    subnet_id = element(module.sas_shared_vpc.intra_subnets, 0)
  }

  ip_address {
    subnet_id = element(module.sas_shared_vpc.intra_subnets, 1)
  }
}

#Endpoint Outbound
resource "aws_route53_resolver_endpoint" "sas_dns_endpoint_out" {
  name      = "sas_dns_endpoint_out"
  direction = "OUTBOUND"

  security_group_ids = [module.sas_shared_security_group.security_group_id]


  ip_address {
    subnet_id = element(module.sas_shared_vpc.intra_subnets, 0)
  }

  ip_address {
    subnet_id = element(module.sas_shared_vpc.intra_subnets, 1)
  }
}

#Endpoint Outbound Rules
resource "aws_route53_resolver_rule" "fwd_rule01" {
  domain_name          = "google.com"
  name                 = "google"
  rule_type            = "FORWARD"
  resolver_endpoint_id = aws_route53_resolver_endpoint.sas_dns_endpoint_out.id

  target_ip {
    ip = "8.8.8.8"
  }
}

#Resolver Rule Association to the VPC
resource "aws_route53_resolver_rule_association" "sas_dns_resolver_association" {
  resolver_rule_id = aws_route53_resolver_rule.fwd_rule01.id
  vpc_id           = module.sas_shared_vpc.vpc_id
}



